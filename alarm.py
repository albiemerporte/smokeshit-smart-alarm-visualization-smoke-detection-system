
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication
from PyQt6.QtCore import QTimer
import random

class AlarmSysMain:
    def __init__(self, inform):
        self.mainui = inform
        self.mainui.show()

        myalarm = int(input("Enter number: "))

        self.timer = QTimer()
        if myalarm == 1:
            self.timer.timeout.connect(self.alarmbedroom)
        if myalarm == 2:
            self.timer.timeout.connect(self.alarmkitchenroom)
        if myalarm == 3:
            self.timer.timeout.connect(self.alarmdirtykitchen)
        if myalarm == 4:
            self.timer.timeout.connect(self.alarmstoraeroom)
        if myalarm == 5:
            self.timer.timeout.connect(self.alarmlivingroom)
        self.timer.start(200)

    def alarmbedroom(self):
        index = int(random.uniform(0, 2))
        #print(index)
        self.mainui.tabGroundFloor.setCurrentIndex(index)

    def alarmkitchenroom(self):
        index = int(random.uniform(2, 4))
        #print(index)
        self.mainui.tabGroundFloor.setCurrentIndex(index)

    def alarmdirtykitchen(self):
        index = int(random.uniform(4, 6))
        #print(index)
        self.mainui.tabGroundFloor.setCurrentIndex(index)

    def alarmstoraeroom(self):
        index = int(random.uniform(6, 8))
        #print(index)
        self.mainui.tabGroundFloor.setCurrentIndex(index)

    def alarmlivingroom(self):
        index = int(random.uniform(8, 10))
        #print(index)
        self.mainui.tabGroundFloor.setCurrentIndex(index)

if __name__ == '__main__':
    app = QApplication([])
    ui = loadUi('GroundFloor.ui')
    main = AlarmSysMain(ui)
    app.exec()